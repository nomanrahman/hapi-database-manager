# HSPC HAPI Database Manager

Welcome to the HSPC HAPI Database Manager!  

### How do I set up?
This project uses Java 11.  Please make sure that your Project SDK is set to use Java 11.

#### Step 1: Preconditions
    To run this project using the mvnw script, you must set up the .mvn folder.  
    The following command will initialize this folder:
    
    mvn -N io.takari:maven:wrapper

#### Step 2: Maven Build
In the terminal, run the following command:

    mvn package

#### Step 3: Run using mvnw
In the terminal, run the following command:

    
    ./mvnw spring-boot:run
    
